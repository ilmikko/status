# Status

Distributed health checking and statistics.

## Features

* Command output checking
	* With regular expressions
	* Static file matching

* TODO: Systemd services
* TODO: File contents (for /sys/class)
* TODO: TLS discussion
* TODO: SMTP/IMAP/POP
* TODO: HTTP status codes
* TODO: HTTP response text
* TODO: HTTPS cert properties
