package config

import (
	"fmt"
	"log"

	"gitlab.com/ilmikko/status/status/check"
)

type Check struct {
	ID     string
	Checks []check.Checkable
}

func (c *Check) Exec(cmd string) error {
	ce, err := check.NewExec(cmd)
	if err != nil {
		return err
	}

	c.Checks = append(c.Checks, ce)
	return nil
}

func (c *Check) File(path string) error {
	cf, err := check.NewFile(path)
	if err != nil {
		return err
	}

	c.Checks = append(c.Checks, cf)
	return nil
}

func (c *Check) Regex(rx string) error {
	cr, err := check.NewRegex(rx)
	if err != nil {
		return err
	}

	c.Checks = append(c.Checks, cr)
	return nil
}

func (c *Check) Run(b []byte) error {
	log.Printf("Running check %q...", c.ID)
	if len(c.Checks) == 0 {
		return fmt.Errorf("%q does not have any checks!", c.ID)
	}
	for _, c := range c.Checks {
		nb, err := c.Check(b)
		if err != nil {
			return err
		}
		b = nb
	}
	return nil
}

func (cfg *Config) getCheck(id string) (*Check, error) {
	c, ok := cfg.checks[id]
	if !ok {
		return nil, fmt.Errorf("Undefined check: %q", id)
	}
	return c, nil
}

func NewCheck(id string) *Check {
	c := &Check{}

	c.ID = id

	return c
}
