package config

import (
	"gitlab.com/ilmikko/status/config/parser"
)

type Config struct {
	parser *parser.Parser
	checks map[string]*Check
	tests  map[string]*Test
}

func Load() (*Config, error) {
	cfg := New()

	if err := cfg.flagLoad(); err != nil {
		return nil, err
	}

	return cfg, nil
}

func New() *Config {
	cfg := &Config{}

	cfg.parser = parser.New()

	cfg.checks = map[string]*Check{}
	cfg.tests = map[string]*Test{}

	return cfg
}

func Init() error {
	flagInit()
	return nil
}
