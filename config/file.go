package config

import (
	"bufio"
	"os"
)

func (cfg *Config) Load(file string, context ...string) error {
	f, err := os.Open(file)
	if err != nil {
		return err
	}
	defer f.Close()

	s := bufio.NewScanner(f)
	for s.Scan() {
		fields, err := cfg.parser.ParseLine(s.Text(), context...)
		if err != nil {
			return err
		}

		if fields == nil {
			continue
		}

		if err := cfg.p(fields); err != nil {
			return err
		}
	}

	return nil
}
