package config

import "flag"

var (
	flagConfigFile = flag.String("config", "", "OVERWATCH CONFIGURATION DIRECTIVE.")
)

func (cfg *Config) flagLoad() error {
	if f := *flagConfigFile; f != "" {
		if err := cfg.Load(f); err != nil {
			return err
		}
	}
	return nil
}

func flagInit() {
	flag.Parse()
}
