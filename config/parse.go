package config

import (
	"fmt"

	"gitlab.com/ilmikko/status/config/parser"
)

func (cfg *Config) pCheckCreate(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.checks[id] = NewCheck(id)
	return nil
}

func (cfg *Config) pCheckExec(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	c, err := cfg.getCheck(id)
	if err != nil {
		return err
	}

	cmd, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := c.Exec(cmd); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pCheckFile(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	c, err := cfg.getCheck(id)
	if err != nil {
		return err
	}

	f, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := c.File(f); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pCheckRegex(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	c, err := cfg.getCheck(id)
	if err != nil {
		return err
	}

	rx, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := c.Regex(rx); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pCheck(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CREATE": cfg.pCheckCreate,
		"EXEC":   cfg.pCheckExec,
		"FILE":   cfg.pCheckFile,
		"REGEX":  cfg.pCheckRegex,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pConfig(fields []string) error {
	l := len(fields)
	file, context := fields[l-1], fields[:l-1]
	if err := cfg.Load(file, context...); err != nil {
		return fmt.Errorf("DIRECTIVE %q: %v", fields, err)
	}
	return nil
}

func (cfg *Config) pRunCheck(fields []string) error {
	testid, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	checkid, err := cfg.parser.Grab1(fields)
	if err != nil {
		return err
	}

	t, err := cfg.getTest(testid)
	if err != nil {
		return err
	}

	c, err := cfg.getCheck(checkid)
	if err != nil {
		return err
	}

	if err := t.RunTestAndCheck(c); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pRunTest(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	t, err := cfg.getTest(id)
	if err != nil {
		return err
	}

	if err := t.RunTest(); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pRun(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CHECK": cfg.pRunCheck,
		"TEST":  cfg.pRunTest,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pTestCreate(fields []string) error {
	id, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	cfg.tests[id] = NewTest(id)
	return nil
}

func (cfg *Config) pTestExec(fields []string) error {
	id, fields, err := cfg.parser.Shift(fields)
	if err != nil {
		return err
	}

	t, err := cfg.getTest(id)
	if err != nil {
		return err
	}

	cmd, err := cfg.parser.GrabAll(fields)
	if err != nil {
		return err
	}

	if err := t.Exec(cmd); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) pTest(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CREATE": cfg.pTestCreate,
		"EXEC":   cfg.pTestExec,
	}); err != nil {
		return err
	}
	return nil
}

func (cfg *Config) p(fields []string) error {
	if err := cfg.parser.Switch(fields, parser.Opts{
		"CHECK":  cfg.pCheck,
		"CONFIG": cfg.pConfig,
		"RUN":    cfg.pRun,
		"TEST":   cfg.pTest,
	}); err != nil {
		return err
	}
	return nil
}
