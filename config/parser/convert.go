package parser

import (
	"fmt"
	"strconv"
)

func (p *Parser) Int(s string) (int, error) {
	n, err := strconv.Atoi(s)
	if err != nil {
		return 0, err
	}
	return n, nil
}

func (p *Parser) IntPositive(s string) (int, error) {
	n, err := p.Int(s)
	if err != nil {
		return 0, err
	}
	if n < 0 {
		return 0, fmt.Errorf("Expected positive integer: %q", s)
	}
	return n, nil
}
