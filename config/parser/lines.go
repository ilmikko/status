package parser

import "strings"

const (
	COMMENT_RUNE = '#'
)

func (p *Parser) IsRule(fields []string) bool {
	// Empty line.
	if len(fields) == 0 {
		return false
	}

	// Comment.
	if fields[0][0] == COMMENT_RUNE {
		return false
	}

	return true
}

func (p *Parser) ParseLine(line string, context ...string) ([]string, error) {
	fields := strings.Fields(line)

	if !p.IsRule(fields) {
		return nil, nil
	}

	// Additional context.
	if len(context) > 0 {
		fields = append(context, fields...)
	}

	return fields, nil
}
