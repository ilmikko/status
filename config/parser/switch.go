package parser

import "fmt"

type Opts map[string]func([]string) error

func (p *Parser) Switch(fields []string, opts Opts) error {
	if err := p.Left(fields); err != nil {
		return err
	}
	cmd, fields := fields[0], fields[1:]
	f, ok := opts[cmd]
	if !ok {
		if err := p.Unknown(cmd); err != nil {
			return err
		}
	}
	if err := f(fields); err != nil {
		return fmt.Errorf("%s: %v", cmd, err)
	}
	return nil
}
