package config

import (
	"bytes"
	"fmt"
	"log"

	"gitlab.com/ilmikko/status/status/run"
)

type Test struct {
	ID   string
	Runs []run.Runnable
}

func (t *Test) Exec(cmd string) error {
	r, err := run.NewExec(cmd)
	if err != nil {
		return err
	}

	t.Runs = append(t.Runs, r)
	return nil
}

func (t *Test) RunTestAndCheck(c *Check) error {
	log.Printf("Running test %q check %q...", t.ID, c.ID)
	buf := &bytes.Buffer{}
	for _, r := range t.Runs {
		rb, err := r.Run()
		if err != nil {
			return err
		}
		if _, err := buf.Write(rb); err != nil {
			return err
		}
	}
	if err := c.Run(buf.Bytes()); err != nil {
		return err
	}
	return nil
}

func (t *Test) RunTest() error {
	log.Printf("Running test %q...", t.ID)
	for _, r := range t.Runs {
		if _, err := r.Run(); err != nil {
			return err
		}
	}
	return nil
}

func (cfg *Config) getTest(id string) (*Test, error) {
	t, ok := cfg.tests[id]
	if !ok {
		return nil, fmt.Errorf("Undefined test: %q", id)
	}
	return t, nil
}

func NewTest(id string) *Test {
	t := &Test{}

	t.ID = id

	return t
}
