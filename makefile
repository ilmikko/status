GITPATH=gitlab.com/ilmikko/status

.PHONY: test

PACKAGES=$(GITPATH)/config\
				 $(GITPATH)/config/parser\
				 $(GITPATH)/status\

# We fake a GOPATH for people that just want to build the module.
build:
	mkdir -p /tmp/go/src/$(shell dirname $(GITPATH))
	test -L /tmp/go/src/$(GITPATH) || ln -sf $(shell pwd) /tmp/go/src/$(GITPATH)
	GOPATH="/tmp/go" go build -o bin/status status.go
	@rm -rf /tmp/go

test:
	go test $(PACKAGES)
