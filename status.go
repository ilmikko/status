package main

import (
	"log"

	"gitlab.com/ilmikko/status/config"
	"gitlab.com/ilmikko/status/status"
)

func mainErr() error {
	if err := config.Init(); err != nil {
		return err
	}

	cfg, err := config.Load()
	if err != nil {
		return err
	}

	s, err := status.New(cfg)
	if err != nil {
		return err
	}

	if err := s.Check(); err != nil {
		return err
	}
	return nil
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("Error: %v", err)
	}
}
