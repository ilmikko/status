package check

type Checkable interface {
	Check([]byte) ([]byte, error)
}
