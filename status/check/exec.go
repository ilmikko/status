package check

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
)

type Exec struct {
	cl []string
}

func (c *Exec) Check(b []byte) ([]byte, error) {
	ex, args := c.cl[0], c.cl[1:]
	cmd := exec.Command(ex, args...)

	stderr := &bytes.Buffer{}
	cmd.Stdin = bytes.NewBuffer(b)
	cmd.Stderr = stderr

	o, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf("Exec: %v: %s", err, stderr.String())
	}

	return o, nil
}

func NewExec(cmd string) (*Exec, error) {
	e := &Exec{}

	e.cl = strings.Fields(cmd)

	return e, nil
}
