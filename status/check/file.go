package check

import (
	"fmt"
	"io/ioutil"
	"reflect"
	"strings"
)

type File struct {
	path string
	data []byte
}

func (r *File) Check(b []byte) ([]byte, error) {
	if !reflect.DeepEqual(r.data, b) {
		return nil, fmt.Errorf("File %q does not match data!\n%s", r.path,
			strings.Join([]string{
				"--   GOT    --",
				string(b),
				"--   WANT   --",
				string(r.data),
				"--   END    --",
			}, "\n"),
		)
	}
	return b, nil
}

func NewFile(path string) (*File, error) {
	f := &File{}

	f.path = path
	data, err := ioutil.ReadFile(f.path)
	if err != nil {
		return nil, err
	}
	f.data = data

	return f, nil
}
