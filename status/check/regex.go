package check

import (
	"fmt"
	"regexp"
	"strings"
)

type Regex struct {
	regexp *regexp.Regexp
}

func (r *Regex) Check(b []byte) ([]byte, error) {
	if !r.regexp.Match(b) {
		return nil, fmt.Errorf("Regex %q does not match data!\n%s", r.regexp,
			strings.Join([]string{
				"--   DATA   --",
				string(b),
				"-- END DATA --",
			}, "\n"),
		)
	}
	return b, nil
}

func NewRegex(rx string) (*Regex, error) {
	r := &Regex{}

	rxp, err := regexp.Compile(rx)
	if err != nil {
		return nil, err
	}

	r.regexp = rxp

	return r, nil
}
