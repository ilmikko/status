package run

import (
	"bytes"
	"fmt"
	"os/exec"
	"strings"
)

type Exec struct {
	cl []string
}

func (r *Exec) Run() ([]byte, error) {
	ex, args := r.cl[0], r.cl[1:]
	cmd := exec.Command(ex, args...)

	stderr := &bytes.Buffer{}
	cmd.Stderr = stderr

	o, err := cmd.Output()
	if err != nil {
		return nil, fmt.Errorf("Exec: %v: %s", err, stderr.String())
	}

	return o, nil
}

func NewExec(cmd string) (*Exec, error) {
	e := &Exec{}

	e.cl = strings.Fields(cmd)

	return e, nil
}
