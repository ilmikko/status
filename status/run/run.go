package run

type Runnable interface {
	Run() ([]byte, error)
}
