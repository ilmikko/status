package status

import (
	"fmt"

	"gitlab.com/ilmikko/status/config"
)

type Status struct{}

func (s *Status) Check() error {
	fmt.Printf("OK\n")
	return nil
}

func New(cfg *config.Config) (*Status, error) {
	s := &Status{}

	return s, nil
}
